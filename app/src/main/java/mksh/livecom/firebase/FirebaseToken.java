package mksh.livecom.firebase;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.orhanobut.hawk.Hawk;

public class FirebaseToken extends FirebaseInstanceIdService {


    public void onTokenRefresh() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Hawk.put("pushToken",refreshedToken);
//        sendRegistrationToServer(refreshedToken);
    }



}
